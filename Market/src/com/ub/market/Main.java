package com.ub.market;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        OrderDoc orderDoc = new OrderDoc();

        for (int i = 1; i <= 20; i++) {
            ItemDoc itemDoc = new ItemDoc();
            Double item = randomInRange(1, 100000);
            try {
                itemDoc.setPrice(item);
            } catch (NotValidDataException e) {
                e.printStackTrace();
            }
            orderDoc.getItems().add(itemDoc);
        }

        NumberFormat formatter = new DecimalFormat("#0.00");

        String changer = formatter.format(orderDoc.getTotalPrice());
        changer = changer.replace(",", ".");

        System.out.println(changer);
        System.out.println(orderDoc.getItems().size());
    }

    protected static Random random = new Random();

    public static double randomInRange(double min, double max) {
        double range = max - min;
        double scaled = random.nextDouble() * range;
        double shifted = scaled + min;
        return shifted; // == (rand.nextDouble() * (max-min)) + min;
    }
}
