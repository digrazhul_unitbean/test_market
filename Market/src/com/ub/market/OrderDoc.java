package com.ub.market;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by digrazhul on 13.05.2015.
 */
public class OrderDoc {
    private List<ItemDoc> items = new ArrayList<ItemDoc>();

    public List<ItemDoc> getItems() {
        return items;
    }

    public void setItems(List<ItemDoc> items) {
        this.items = items;
    }

    public Double getTotalPrice() {


        Double tmp = 0.;
        for (ItemDoc itemDoc : items) {
            tmp = tmp + itemDoc.getPrice();
        }

        return tmp;
    }
}
