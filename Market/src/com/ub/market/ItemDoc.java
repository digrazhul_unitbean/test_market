package com.ub.market;

/**
 * Created by digrazhul on 13.05.2015.
 */
public class ItemDoc {
    private Double price;


    public Double getPrice() {

        return price;
    }

    public void setPrice(Double price) throws NotValidDataException {
        if (price < 0){
            throw new NotValidDataException();
        }
        this.price = price;
    }
}
